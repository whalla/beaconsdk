//
//  WHARegionProtocol.h
//  beacons
//
//  Created by Marcin Gorny on 25.06.2015.
//  Copyright (c) 2015 Whallalabs. All rights reserved.
//

@import Foundation;
@import CoreLocation;

typedef void (^EnterRegionCompletionBlock)(CLRegion *region);
typedef void (^RangeChangeCompletionBlock)(CLRegion *region, NSArray *beacons);

@protocol WHARegionProtocol <NSObject>

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, strong) EnterRegionCompletionBlock onEnterCompletionBlock;
@property (nonatomic, strong) EnterRegionCompletionBlock onExitCompletionBlock;

@optional
@property (nonatomic, strong) RangeChangeCompletionBlock rangeChangedCompletionBlock;
@property (nonatomic, strong) NSString *proximityUUID;

@end
