
//
//  BeaconsHandler.m
//  beacons
//
//  Created by Marcin Gorny on 22.06.2015.
//  Copyright (c) 2015 Whallalabs. All rights reserved.
//

#import "RegionsSDK.h"
#import "WHARegionProtocol.h"
#import "WHABeacon.h"
#import "WHALocation.h"

@interface RegionsSDK () <CLLocationManagerDelegate>

@end

@implementation RegionsSDK

+ (RegionsSDK *)sharedInstance {
    static RegionsSDK *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RegionsSDK alloc] init];
    });
    return instance;
}

- (void)startMonitoringRegion:(CLRegion *)region {
    region.notifyOnEntry = YES;
    if ([CLLocationManager isRangingAvailable]) {
        [self.locationManager startMonitoringForRegion:region];
        [self.locationManager startUpdatingLocation];
    }
}

- (void)startMonitoringGeoRegion:(CLLocationCoordinate2D)location radius:(NSInteger)radius {
    CLCircularRegion *geoFencingRegion = [[CLCircularRegion alloc] initWithCenter:location radius:radius identifier:@"Polna"];
    [self.locationManager startMonitoringForRegion:geoFencingRegion];
}

- (void)stopMonitoringBeacons:(NSString *)identifier {
}

- (void)stopMonitoringRegions:(NSString *)identifier {
    NSArray *allMonitoredRegions = [self.locationManager.monitoredRegions allObjects];
    [allMonitoredRegions enumerateObjectsUsingBlock:^(CLRegion *region, NSUInteger idx, BOOL *stop) {
        if ([region.identifier isEqualToString:identifier]) {
            [self.locationManager stopMonitoringForRegion:region];
        }
    }];
}

- (NSArray *)monitoredBeacons {
    return [self.locationManager.monitoredRegions allObjects];
}

#pragma mark - CLLocationDelegate

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSArray *filteredRegions = [self.regions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", region.identifier]];
    if (filteredRegions.count > 0) {
        [filteredRegions enumerateObjectsUsingBlock:^(id < WHARegionProtocol > obj, NSUInteger idx, BOOL *stop) {
            if (obj.onEnterCompletionBlock) {
                obj.onEnterCompletionBlock(region);
            }
        }];
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    NSArray *filteredRegions = [self.regions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", region.identifier]];
    if (filteredRegions.count > 0) {
        [filteredRegions enumerateObjectsUsingBlock:^(id < WHARegionProtocol > obj, NSUInteger idx, BOOL *stop) {
            if (obj.rangeChangedCompletionBlock) {
                obj.rangeChangedCompletionBlock(region, beacons);
            }
        }];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSArray *filteredRegions = [self.regions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", region.identifier]];
    if (filteredRegions.count > 0) {
        [filteredRegions enumerateObjectsUsingBlock:^(id < WHARegionProtocol > obj, NSUInteger idx, BOOL *stop) {
            if (obj.onExitCompletionBlock) {
                obj.onExitCompletionBlock(region);
            }
            [self.locationManager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
        }];
    }
}

#pragma mark - failure handlers
- (void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.locationManager requestStateForRegion:region];
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    NSArray *filteredRegions = [self.regions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", region.identifier]];
    if (filteredRegions.count > 0) {
        [filteredRegions enumerateObjectsUsingBlock:^(id < WHARegionProtocol > obj, NSUInteger idx, BOOL *stop) {
            if ([region isKindOfClass:[CLBeaconRegion class]]) {
                if (obj.rangeChangedCompletionBlock) {
                    if (state == CLRegionStateInside) {
                        [self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
                    } else {
                        [self.locationManager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
                    }
                }
            }
        }];
    }
}

- (void)addBeacon:(WHABeacon *)region {
    NSMutableArray *mutableRegions = [[NSMutableArray alloc] initWithArray:self.regions];
    NSArray *filteredArray = [self.regions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"proximityUUID == %@", region.proximityUUID]];
    if (filteredArray.count == 0) {
        [mutableRegions addObject:region];
        CLBeaconRegion *nativeRegion;
        if (region.major > 0) {
            if (region.minor > 0) {
                nativeRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:region.proximityUUID] major:region.major minor:region.minor identifier:region.identifier];
            } else {
                nativeRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:region.proximityUUID] major:region.major identifier:region.identifier];
            }
        } else {
            nativeRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:region.proximityUUID] identifier:region.identifier];
        }
        
        [self startMonitoringRegion:nativeRegion];
        self.regions = [mutableRegions copy];
    }
}

- (void)addLocation:(WHALocation *)region {
    NSMutableArray *mutableRegions = [[NSMutableArray alloc] initWithArray:self.regions];
    [mutableRegions addObject:region];
    CLCircularRegion *nativeRegion = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(1, 1) radius:1 identifier:region.identifier];
    [self startMonitoringRegion:nativeRegion];
    self.regions = [mutableRegions copy];
}

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
    }
    return _locationManager;
}

- (NSArray *)regions {
    if (!_regions) {
        _regions = [[NSArray alloc] init];
    }
    return _regions;
}

@end
