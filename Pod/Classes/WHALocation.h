//
//  WHALocationHandler.h
//  beacons
//
//  Created by Marcin Gorny on 25.06.2015.
//  Copyright (c) 2015 Whallalabs. All rights reserved.
//

@import Foundation;
@class RegionsSDK;

#import "WHARegionProtocol.h"

@interface WHALocation : NSObject <WHARegionProtocol>

@property (nonatomic, copy) NSString *identifier;

@property (nonatomic, strong) EnterRegionCompletionBlock onEnterCompletionBlock;
@property (nonatomic, strong) EnterRegionCompletionBlock onExitCompletionBlock;

- (void)setOnEnterCompletionBlock:(EnterRegionCompletionBlock)onEnterCompletionBlock;
- (void)setOnExitCompletionBlock:(EnterRegionCompletionBlock)onExitCompletionBlock;



@end
