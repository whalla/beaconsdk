//
//  BeaconsHandler.h
//  beacons
//
//  Created by Marcin Gorny on 22.06.2015.
//  Copyright (c) 2015 Whallalabs. All rights reserved.
//

@import Foundation;
@import CoreLocation;
@import UIKit;

#import "WHABeacon.h"
#import "WHALocation.h"

@interface RegionsSDK : NSObject
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *regions;

- (NSArray *)monitoredBeacons;
- (void)addBeacon:(WHABeacon *)region;
- (void)addLocation:(WHALocation *)region;

- (void)stopMonitoringBeacons:(NSString *)identifier;
- (void)stopMonitoringRegions:(NSString *)identifier;

+ (RegionsSDK *)sharedInstance;

@end
