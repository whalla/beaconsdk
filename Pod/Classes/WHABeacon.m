//
//  WHABeaconHandler.m
//  beacons
//
//  Created by Marcin Gorny on 25.06.2015.
//  Copyright (c) 2015 Whallalabs. All rights reserved.
//

#import "WHABeacon.h"
@interface WHABeacon ()

@end


@implementation WHABeacon
- (id)initWithIdentifier:(NSString *)identifier proximityUUID:(NSString *)proximitiUUID {
    self = [self initWithIdentifier:identifier proximityUUID:proximitiUUID major:0];
    return self;
}

- (id)initWithIdentifier:(NSString *)identifier proximityUUID:(NSString *)proximitiUUID major:(NSInteger)major {
    self = [self initWithIdentifier:identifier proximityUUID:proximitiUUID major:major minor:0];
    return self;
}

- (id)initWithIdentifier:(NSString *)identifier proximityUUID:(NSString *)proximitiUUID major:(NSInteger)major minor:(NSInteger)minor {
    self = [super init];
    self.identifier = identifier;
    self.proximityUUID = proximitiUUID;
    self.major = major;
    self.minor = minor;
    return self;
}

@end
