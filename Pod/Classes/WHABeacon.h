//
//  WHABeaconHandler.h
//  beacons
//
//  Created by Marcin Gorny on 25.06.2015.
//  Copyright (c) 2015 Whallalabs. All rights reserved.
//

@import Foundation;
@class RegionsSDK;

#import "WHARegionProtocol.h"

@interface WHABeacon : NSObject <WHARegionProtocol>

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, strong) NSString *proximityUUID;
@property (nonatomic, readwrite) NSInteger major;
@property (nonatomic, readwrite) NSInteger minor;

@property (nonatomic, strong) EnterRegionCompletionBlock onEnterCompletionBlock;
@property (nonatomic, strong) EnterRegionCompletionBlock onExitCompletionBlock;
@property (nonatomic, strong) RangeChangeCompletionBlock rangeChangedCompletionBlock;

- (id)initWithIdentifier:(NSString *)identifier proximityUUID:(NSString *)proximitiUUID;
- (id)initWithIdentifier:(NSString *)identifier proximityUUID:(NSString *)proximitiUUID major:(NSInteger)major;
- (id)initWithIdentifier:(NSString *)identifier proximityUUID:(NSString *)proximitiUUID major:(NSInteger)major minor:(NSInteger)minor;

- (void)setOnEnterCompletionBlock:(EnterRegionCompletionBlock)onEnterCompletionBlock;
- (void)setOnExitCompletionBlock:(EnterRegionCompletionBlock)onExitCompletionBlock;
- (void)setRangeChangedCompletionBlock:(RangeChangeCompletionBlock)rangeChangedCompletionBlock;
@end
