# beaconSDK

[![CI Status](http://img.shields.io/travis/Marcin Gorny/beaconSDK.svg?style=flat)](https://travis-ci.org/Marcin Gorny/beaconSDK)
[![Version](https://img.shields.io/cocoapods/v/beaconSDK.svg?style=flat)](http://cocoapods.org/pods/beaconSDK)
[![License](https://img.shields.io/cocoapods/l/beaconSDK.svg?style=flat)](http://cocoapods.org/pods/beaconSDK)
[![Platform](https://img.shields.io/cocoapods/p/beaconSDK.svg?style=flat)](http://cocoapods.org/pods/beaconSDK)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

beaconSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "beaconSDK"
```

## Author

Marcin Gorny, marcin.gorny@gmail.com

## License

beaconSDK is available under the MIT license. See the LICENSE file for more info.
